<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Nurseries</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	</head>
  	<body onload="load();">    
  		<b>Add a Nursery:</b><br/>
		<input type="hidden" id="id_nursery">
		Name: <input type="text" id="name" required="required"	name="name"><br>
		<button onclick="submit();">Add</button>
		<table id="table" border=1>
			<tr> <th> Name </th> <th> Edit </th> <th> Delete </th> </tr>
		</table>
		
		<script type="text/javascript">
			data = "";
			submit = function(){
				$.ajax({
					url:'saveOrUpdate',
					type:'POST',
					data:{idNursery:$("#id_nursery").val(),name:$('#name').val()},					success: function(response){
						alert(response.message);
						load();
					}
				});
			    
			}
			
			delete_ = function(id){
				$.ajax({
					url:'delete',
					type:'POST',
					data:{idNursery:id},
					success: function(response){
						alert(response.message);
						load();
					}
				});
			}
			
			edit = function (index){
				$("#id_nursery").val(data[index].idNursery);
				$("#name").val(data[index].nurseryName);
			}
			
			load = function(){
				$.ajax({
					url:'list',
					type:'POST',
					success: function(response){
						data = response.data;
						console.log(data);
						$('.tr').remove();
						for(i=0; i<response.data.length; i++){
							$("#table").append("<tr class='tr'> <td>"
								+response.data[i].nurseryName+" </td> <td>	<a href='#' onclick= edit("
								+i+");> Edit </a> </td> </td> <td> <a href='#'onclick='delete_("+response.data[i].idNursery+");'> Delete </a> </td> </tr>");
						}
					}
				});
			}
		</script>
	</body>
</html>