<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Children</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	</head>
  	<body onload="load();">    
  		<b>Add a child:</b><br/>
		<input type="hidden" id="id_child">
		First name: <input type="text" id="firstname" required="required"	name="firstname"><br>
		Last name: <input type="text" id="lastname" required="required" name="lastname"><br>
		Birth date: <input type="date" id="birthdate" required="required" name="birthdate"><br>
		<button onclick="submit();">Add</button>
		<table id="table" border=1>
			<tr> <th> First Name </th> <th> Last Name </th> <th> Birth Date </th> <th> Edit </th> <th> Delete </th> </tr>
		</table>
		
		<script type="text/javascript">
			data = "";
			submit = function(){
				var docfirstname = document.getElementById("firstname");
			    var doclastname = document.getElementById("lastname");
			    var docbirthdate = new Date($('#birthdate').val());
			    if(!docfirstname.value || !doclastname.value || !docbirthdate){
			        alert('One field is empty');
			    }
			    else{
					$.ajax({
						url:'saveOrUpdate',
						type:'POST',
						data:{idChild:$("#id_child").val(),firstname:$('#firstname').val(),
						lastname:$('#lastname').val(),birthdate:docbirthdate},
						success: function(response){
							alert(response.message);
							load();
						}
					});
			    }
			}
			
			delete_ = function(id){
				$.ajax({
					url:'delete',
					type:'POST',
					data:{idChild:id},
					success: function(response){
						alert(response.message);
						load();
					}
				});
			}
			
			edit = function (index){
				$("#idChild").val(data[index].idChild);
				$("#firstname").val(data[index].firstname);
				$("#lastname").val(data[index].lastname);
				$("#birthdate").val(data[index].birthdate);
			}
			
			load = function(){
				$.ajax({
					url:'list',
					type:'POST',
					success: function(response){
						data = response.data;
						console.log(data);
						$('.tr').remove();
						for(i=0; i<response.data.length; i++){
							$("#table").append("<tr class='tr'> <td>"
								+response.data[i].firstname+" </td> <td> "
								+response.data[i].lastname+" </td> <td> "
								+response.data[i].birthdate+" </td> <td>	<a href='#' onclick= edit("
								+i+");> Edit </a> </td> </td> <td> <a href='#'onclick='delete_("+response.data[i].idChild+");'> Delete </a> </td> </tr>");
						}
					}
				});
			}
		</script>
	</body>
</html>