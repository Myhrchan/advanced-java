<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Offdays</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	</head>
  	<body onload="load();"> 
  		<a href="/javaproject-champalier-coton/nurseries/page">Nurseries</a>
		<a href="/javaproject-champalier-coton/children/page">Children</a>
		<a href="/javaproject-champalier-coton/contracts/page">Contracts</a>
		<a href="/javaproject-champalier-coton/vacations/page">Vacations</a>
		<a href="/javaproject-champalier-coton/offdays/page">Off-days</a>
		<a href="/javaproject-champalier-coton/absences/page">Absences</a>
		<a href="/javaproject-champalier-coton/reservations/page">Reservations</a>
		<a href="/javaproject-champalier-coton/cancellations/page">Cancellations</a><br><br>
  	
  		<h1>Days off</h1>  <br>   
  		<b>Add a day off:</b><br/>
		Nursery name: <select id="nursery">
		</select><br>
		Date: <input type="date" id="date" required="required" name="date"><br>
		Period: 
		<select id="period">
			<option value="morning" selected>Morning</option>
			<option value="afternoon">Afternoon</option>
		</select><br>
		<button onclick="submit();">Add</button>
		<table id="table" border=1>
			<tr> <th> Nursery name</th> <th> Date </th> <th> Period </th> <th> Edit </th> <th> Delete </th> </tr>
		</table>
		
		<script type="text/javascript">
			data = "";
			
			function saveDate() {
				return $.ajax({
					url:'saveOrUpdateDate',
					type:'POST',
					data:{
						offdayDay:$('#date').val()
					},
					success: function(response){
						save1();
					}
				});
			}
			
			function save1() {
				return $.ajax({
					url:'saveOrUpdate1',
					type:'POST',
					data:{
							offdayPeriod:$("#period").val()
					},
					success: function(response){
						save2();
					}
				});
			}
			
			function save2() {
				return $.ajax({
					url:'saveOrUpdate2',
					type:'POST',
					data:{
							nurseryName:$("#nursery").val()
					},
					success: function(response){
						alert(response.message);
						loadbis();
					}
				});
			}
			
			submit = function(){
				var docnursery = document.getElementById("nursery").value;
			    var docdate = document.getElementById("date").value;
			    if(!docnursery || !docdate){
			        alert('One field is empty');
			    }
			    else{
					saveDate().success();
			    }
			}
			
			
			delete_ = function(i){
			//	alert(data[i].id.nurseryId);
				$.ajax({
					url:'delete',
					type:'POST',
					data:{offdayPeriod: data[i].id.offdayPeriod, offdayDay: data[i].id.offdayDay, idNursery:  data[i].id.idNursery},
					success: function(response){
						alert(response.message);
						loadbis();
					}
				});
			}
			
			edit = function (index){
				$("#nursery").val(data[index].nurseries.nurseryName);
				$("#date").val(data[index].id.offdayDay);
			}
			
			load = function(){
				
				$.ajax({
					url:'listNurseries',
					type:'POST',
					success: function(response){
						data = response.data;
						console.log(data);
						$('.tr').remove();
						for(i=0; i<response.data.length; i++){
							$("#nursery").append("<option value="+response.data[i].nurseryName+">"+response.data[i].nurseryName+"</option>");
						}
					}
				});
				
				loadbis();
			}
			
			function loadbis(){
				$.ajax({
					url:'list',
					type:'POST',
					success: function(response){
						data = response.data;
						console.log(data);
						$('.tr').remove();
						for(i=0; i<response.data.length; i++){
							$("#table").append("<tr class='tr'> <td>"
								+response.data[i].nurseries.nurseryName+" </td> <td> "
								+response.data[i].id.offdayDay+" </td> <td> "
								+response.data[i].id.offdayPeriod+" </td> <td>	<a href='#' onclick= edit("
								+i+");> Edit </a> </td> </td> <td> <a href='#'onclick='delete_("+i+");'> Delete </a> </td> </tr>");
						}
					}
				});
			}
		</script>
	</body>
</html>