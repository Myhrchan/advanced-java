<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Contracts</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	</head>
  	<body onload="load();">  
  		<a href="/javaproject-champalier-coton/nurseries/page">Nurseries</a>
		<a href="/javaproject-champalier-coton/children/page">Children</a>
		<a href="/javaproject-champalier-coton/contracts/page">Contracts</a>
		<a href="/javaproject-champalier-coton/vacations/page">Vacations</a>
		<a href="/javaproject-champalier-coton/offdays/page">Off-days</a>
		<a href="/javaproject-champalier-coton/absences/page">Absences</a>
		<a href="/javaproject-champalier-coton/reservations/page">Reservations</a>
		<a href="/javaproject-champalier-coton/cancellations/page">Cancellations</a><br><br>
  	
  		<h1>Contracts</h1>  <br>
		Create new contract:  Please fill all the fields<br>
		<input type="hidden" id="contract_id">
		Child: <select id="children"></select><br>
		Nursery: <select id="nursery"></select><br>
		Phone: <input type="text" id="phone" required="required" name="phone"><br>
		Email: <input type="text" id="email" required="required" name="email"><br>
		Price per hour: <input type="text" id="price" required="required" name="price"><br>
		Start date: <input type="date" id="startdate" required="required" name="startdate"><br>
		End date: <input type="date" id="enddate" required="required" name="enddate"><br>
		Type of contract: <br>
		<input id="regular" type="radio" name="contract" onclick="appearFunction()">
		<label for="regular">Regular</label><br>
		<input id="temporary" type="radio" name="contract" onclick="disappearFunction()">
		<label for="temporary">Temporary</label><br>
		
		<div id="regularinfo">
			Day 1:  <select id="day1">
			  <option value="null" selected>null</option>
			  <option value="monday">Monday</option>
			  <option value="tuesday">Tuesday</option>
			  <option value="wednesday">Wednesday</option>
			  <option value="thursday">Thursday</option>
			  <option value="friday">Friday</option>
			  <option value="saturday">Saturday</option>
			  <option value="sunday">Sunday</option>
			</select><br>
			Period 1: 
			<select id="period1">
			  <option value="null" selected>null</option>
			  <option value="morning">Morning</option>
			  <option value="afternoon">Afternoon</option>
			</select><br>
			Day 2: <select id="day2">
			  <option value="null" selected>null</option>
			  <option value="monday">Monday</option>
			  <option value="tuesday">Tuesday</option>
			  <option value="wednesday">Wednesday</option>
			  <option value="thursday">Thursday</option>
			  <option value="friday">Friday</option>
			  <option value="saturday">Saturday</option>
			  <option value="sunday">Sunday</option>
			</select><br>
			Period 2: 
			<select id="period2">
			  <option value="null" selected>null</option>
			  <option value="morning">Morning</option>
			  <option value="afternoon">Afternoon</option>
			</select><br>
			Day 3:  <select id="day3">
			  <option value="null" selected>null</option>
			  <option value="monday">Monday</option>
			  <option value="tuesday">Tuesday</option>
			  <option value="wednesday">Wednesday</option>
			  <option value="thursday">Thursday</option>
			  <option value="friday">Friday</option>
			  <option value="saturday">Saturday</option>
			  <option value="sunday">Sunday</option>
			</select><br>
			Period 3: 
			<select id="period3">
			  <option value="null" selected>null</option>
			  <option value="morning">Morning</option>
			  <option value="afternoon">Afternoon</option>
			</select><br>
			Day 4:  <select id="day4">
			  <option value="null" selected>null</option>
			  <option value="monday">Monday</option>
			  <option value="tuesday">Tuesday</option>
			  <option value="wednesday">Wednesday</option>
			  <option value="thursday">Thursday</option>
			  <option value="friday">Friday</option>
			  <option value="saturday">Saturday</option>
			  <option value="sunday">Sunday</option>
			</select><br>
			Period 4: 
			<select id="period4">
			  <option value="null" selected>null</option>
			  <option value="morning">Morning</option>
			  <option value="afternoon">Afternoon</option>
			</select><br>
		</div>
		
		
		<button onclick="submit();">Submit</button><br><br>
		<table id="table" border=1>
			<tr> <th> First Name </th> <th> Last Name </th> <th> Nursery </th>  <th> Phone </th>
			<th> Email </th> <th> Price per hour </th> <th> Start date </th> <th> End date </th> 
			<th> Day 1 </th> <th> Period 1 </th> <th> Day 2 </th> <th> Period 2 </th> 
			<th> Day 3 </th> <th> Period 3 </th> <th> Day 4 </th> <th> Period 4 </th>
			 <th> Edit </th> <th> Delete </th> </tr>
		</table>
		
		<script type="text/javascript">
			data = "";
			
			function save1(){
				
				var children = {
						idChild:$('#children').val()
					};
								
				return $.ajax({
					url:'saveOrUpdate1',
					type:'POST',
					data:children,
					success: function(response){
						saveDate();
					}
				});
			}
			
			function save2(){
				
				var nurseries = {
					nurseryName:$('#nursery').val()
				};
								
				return $.ajax({
					url:'saveOrUpdate2',
					type:'POST',
					data:nurseries,
					success: function(response){
						save3();
					}
				});
			}
			
			function save3(){
						
				var docphone = document.getElementById("phone").value;
			    var docemail = document.getElementById("email").value;
			    var docprice = document.getElementById("email").value;
			    var docstartdate = document.getElementById("startdate").value;
			    var docenddate = document.getElementById("enddate").value;
			    if(!docphone || !docemail || !docprice || !docenddate || !docstartdate){
			        alert('One field is empty');
			    }
			    else{

			    	var i = document.getElementById("day1");
					var day1 = i.options[i.selectedIndex].value;
					var i = document.getElementById("day2");
					var day2 = i.options[i.selectedIndex].value;
					var i = document.getElementById("day3");
					var day3 = i.options[i.selectedIndex].value;
					var i = document.getElementById("day4");
					var day4 = i.options[i.selectedIndex].value;
					var i = document.getElementById("period1");
					var period1 = i.options[i.selectedIndex].value;
					var i = document.getElementById("period2");
					var period2 = i.options[i.selectedIndex].value;
					var i = document.getElementById("period3");
					var period3 = i.options[i.selectedIndex].value;
					var i = document.getElementById("period4");
					var period4 = i.options[i.selectedIndex].value;

					var contract = {
						idContract:$("#contract_id").val(),
						phone:$('#phone').val(),email:$('#email').val(),pricePerHour:$('#price').val(),
						date1:day1, date2:day2, date3:day3, date4:day4,
						period1:period1, period2:period2, period3:period3, period4:period4
					};
					
					
					return $.ajax({
						url:'saveOrUpdate3',
						type:'POST',
						data:contract,
						success: function(response){
							alert(response.message);
							loadbis();
						}
					});
			    }
			}
			
			function saveDate(){
				return $.ajax({
					url:'saveOrUpdateDate',
					type:'POST',
					data:{
						startDate:$('#startdate').val(), endDate:$('#enddate').val()
					},
					success: function(response){
						save2();
					}
				});
			}
			
			
			submit = function(){
				save1();
			}
			
			delete_ = function(id){
				$.ajax({
					url:'delete',
					type:'POST',
					data:{idContract:id},
					success: function(response){
						alert(response.message);
						loadbis();
					}
				});
			}
			
			edit = function (index){
				$("#contract_id").val(data[index].idContract);
				$("#children").val(data[index].children.idChild);
				$("#nursery").val(data[index].nurseries.nurseryName);
				$("#phone").val(data[index].phone);
				$("#email").val(data[index].email);
				$("#price").val(data[index].pricePerHour);
				$("#startdate").val(data[index].startDate);
				$("#enddate").val(data[index].endDate);
				if(data[i].date1 !== undefined){
					radiobtn = document.getElementById("regular");
					radiobtn.checked = true;
					$("#day1").val(data[index].date1);
					$("#day2").val(data[index].date2);
					$("#day3").val(data[index].date3);
					$("#day4").val(data[index].date4);
					$("#period1").val(data[index].period1);
					$("#period2").val(data[index].period2);
					$("#period3").val(data[index].period3);
					$("#period4").val(data[index].period4);
				}
				else{
					radiobtn = document.getElementById("temporary");
					radiobtn.checked = true;
				}
			}
			
			load = function(){
				disappearFunction();
				

				$.ajax({
					url:'listChildren',
					type:'POST',
					success: function(response){
						data = response.data;
						console.log(data);
						$('.tr').remove();
						for(i=0; i<response.data.length; i++){
							$("#children").append("<option value="+response.data[i].idChild+">"+response.data[i].firstname+" "+response.data[i].lastname+"</option>");
						}
					}
				});
				

				$.ajax({
					url:'listNurseries',
					type:'POST',
					success: function(response){
						data = response.data;
						console.log(data);
						$('.tr').remove();
						for(i=0; i<response.data.length; i++){
							$("#nursery").append("<option value="+response.data[i].nurseryName+">"+response.data[i].nurseryName+"</option>");
						}
						
						loadbis();
					}
				});
				
			}
			
			function loadbis(){
				$.ajax({
					url:'list',
					type:'POST',
					success: function(response){
						data = response.data;
						console.log(data);
						$('.tr').remove();
						for(i=0; i<response.data.length; i++){
							$("#table").append("<tr class='tr'> <td>"+response.data[i].children.firstname+" </td><td>"+response.data[i].children.lastname+" </td> <td>"
								+response.data[i].nurseries.nurseryName+" </td><td>"+response.data[i].phone+" </td><td> "
								+response.data[i].email+" </td> <td>"+response.data[i].pricePerHour+" </td> <td>"
								+response.data[i].startDate+" </td> <td>"+response.data[i].endDate+" </td> <td>"
								+response.data[i].date1+" </td> <td>"+response.data[i].period1+" </td> <td>"
								+response.data[i].date2+" </td> <td>"+response.data[i].period2+" </td> <td>"
								+response.data[i].date3+" </td> <td>"+response.data[i].period3+" </td> <td>"
								+response.data[i].date4+" </td> <td>"+response.data[i].period4
								+" </td><td>	<a href='#' onclick= edit("
								+i+");> Edit </a> </td> </td> <td> <a href='#'onclick='delete_("+response.data[i].idContract+");'> Delete </a> </td> </tr>");
						}
					}
				});
			}
			
			function appearFunction(){
				var x = document.getElementById("regularinfo");
			    x.style.display = "block";
			}
			
			function disappearFunction() {
			    var x = document.getElementById("regularinfo");
		        x.style.display = "none";
			} 
		</script>
	</body>
</html>