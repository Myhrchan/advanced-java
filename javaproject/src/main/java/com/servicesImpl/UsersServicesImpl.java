package com.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.UsersDao;
import com.entities.Users;
import com.services.UsersServices;

@Repository
@Transactional
public class UsersServicesImpl implements UsersServices {

	@Autowired
	UsersDao usersDao;
	
	public boolean saveOrUpdate(Users users) {
		return usersDao.saveOrUpdate(users);
	}

	public List<Users> list() {
		return usersDao.list();
	}

	public boolean delete(Users users) {
		return usersDao.delete(users);
	}
}
