package com.servicesImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.OffdaysDao;
import com.entities.Nurseries;
import com.entities.Offdays;
import com.services.OffdaysServices;

@Repository
@Transactional
public class OffdaysServicesImpl implements OffdaysServices {

	@Autowired
	OffdaysDao offdaysDao;
	
	public boolean saveOrUpdate(Offdays offdays) {
		return offdaysDao.saveOrUpdate(offdays);
	}

	public List<Offdays> list() {
		return offdaysDao.list();
	}

	public boolean delete(Offdays offdays) {
		return offdaysDao.delete(offdays);
	}
	
	public int getNurseryFromName(String nurseryName) {
		return offdaysDao.getNurseryFromName(nurseryName).getIdNursery();
	}

	public List<Nurseries> getNurseries() {
		return offdaysDao.getNurseries();
	}

	public Offdays findOffdayById(String offdayPeriod, Date date, Integer idNursery) {
		return offdaysDao.findOffdayById(offdayPeriod, date, idNursery);
	}
	
	
}
