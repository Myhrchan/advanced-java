package com.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.NurseriesDao;
import com.entities.Nurseries;
import com.services.NurseriesServices;

@Repository
@Transactional
public class NurseriesServicesImpl implements NurseriesServices {

	@Autowired
	NurseriesDao nurseriesDao;
	
	public boolean saveOrUpdate(Nurseries nurseries) {
		return nurseriesDao.saveOrUpdate(nurseries);
	}

	public List<Nurseries> list() {
		return nurseriesDao.list();
	}

	public boolean delete(Nurseries nurseries) {
		return nurseriesDao.delete(nurseries);
	}
}
