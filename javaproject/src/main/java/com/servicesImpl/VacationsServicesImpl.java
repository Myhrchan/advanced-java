package com.servicesImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.VacationsDao;
import com.entities.Children;
import com.entities.Contracts;
import com.entities.Vacations;
import com.services.VacationsServices;

@Repository
@Transactional
public class VacationsServicesImpl implements VacationsServices {

	@Autowired
	VacationsDao vacationsDao;
	
	public boolean saveOrUpdate(Vacations vacations) {
		return vacationsDao.saveOrUpdate(vacations);
	}

	public List<Vacations> list() {
		return vacationsDao.list();
	}

	public boolean delete(Vacations vacations) {
		return vacationsDao.delete(vacations);
	}

	public List<Children> getChildren() {
		return vacationsDao.getChildren();
	}

	public boolean isThereARegularContractForThisChild(Integer idChild) {
		return vacationsDao.isThereARegularContractForThisChild(idChild);
	}

	public Contracts getRegularContractByChildId(Integer idChild) {
		return vacationsDao.getRegularContractByChildId(idChild);
	}

	public Vacations findVacationsById(String vacationsPeriod, Date vacationsDay, Integer idContract) {
		return vacationsDao.findVacationsById(vacationsPeriod, vacationsDay, idContract);

	}
}
