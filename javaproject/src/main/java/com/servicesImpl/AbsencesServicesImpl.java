package com.servicesImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.AbsencesDao;
import com.entities.Absences;
import com.entities.Children;
import com.entities.Contracts;
import com.services.AbsencesServices;

@Repository
@Transactional
public class AbsencesServicesImpl implements AbsencesServices {

	@Autowired
	AbsencesDao absencesDao;
	
	public boolean saveOrUpdate(Absences absences) {
		return absencesDao.saveOrUpdate(absences);
	}

	public List<Absences> list() {
		return absencesDao.list();
	}

	public boolean delete(Absences absences) {
		return absencesDao.delete(absences);
	}

	public List<Children> getChildren() {
		return absencesDao.getChildren();
	}

	public Absences findAbsenceById(String absencesPeriod, Date date, Integer idContract) {
		return absencesDao.findAbsenceById(absencesPeriod, date, idContract);
	}
	
	public Contracts getContractByIdChild(Integer idChild) {
		return absencesDao.getContractByIdChild(idChild);
	}

}
