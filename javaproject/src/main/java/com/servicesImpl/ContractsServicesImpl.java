package com.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.ContractsDao;
import com.entities.Children;
import com.entities.Contracts;
import com.entities.Nurseries;
import com.services.ContractsServices;

@Repository
@Transactional
public class ContractsServicesImpl implements ContractsServices {

	@Autowired
	ContractsDao contractsDao;
	
	public boolean saveOrUpdate(Contracts users) {
		return contractsDao.saveOrUpdate(users);
	}

	public List<Contracts> list() {
		return contractsDao.list();
	}

	public boolean delete(Contracts users) {
		return contractsDao.delete(users);
	}
	
	public Nurseries getNurseryFromName(String name) {
		return contractsDao.getNurseryFromName(name);
	}
	
	
	public Children getChildrenFromName(String firstname, String lastname) {
		return contractsDao.getChildrenFromName(firstname, lastname);
	}
	
	public List<Children> getChildren(){
		return contractsDao.getChildren();
	}

	public List<Nurseries> getNurseries(){
		return contractsDao.getNurseries();
	}
}
