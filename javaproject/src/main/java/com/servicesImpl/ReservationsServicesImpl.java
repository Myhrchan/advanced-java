package com.servicesImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.ReservationsDao;
import com.entities.Children;
import com.entities.Contracts;
import com.entities.Reservations;
import com.services.ReservationsServices;

@Repository
@Transactional
public class ReservationsServicesImpl implements ReservationsServices {

	@Autowired
	ReservationsDao reservationsDao;
	
	public boolean saveOrUpdate(Reservations offdays) {
		return reservationsDao.saveOrUpdate(offdays);
	}

	public List<Reservations> list() {
		return reservationsDao.list();
	}

	public boolean delete(Reservations offdays) {
		return reservationsDao.delete(offdays);
	}
	
	public int getNurseryFromName(String nurseryName) {
		return reservationsDao.getNurseryFromName(nurseryName).getIdNursery();
	}

	public List<Children> getChildren() {
		return reservationsDao.getChildren();
	}

	public Contracts getTemporaryContractByChildId(Integer idChild) {
		return reservationsDao.getTemporaryContractByChildId(idChild);
	}

	public boolean isThereATemporaryContractForThisChild(Integer idChild) {
		return reservationsDao.isThereATemporaryContractForThisChild(idChild);
	}

	public Reservations findReservationById(String reservationsPeriod, Date date, Integer idContract) {
		return reservationsDao.findReservationById(reservationsPeriod, date, idContract);
	}
}
