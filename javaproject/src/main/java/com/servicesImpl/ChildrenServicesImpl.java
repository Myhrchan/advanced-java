package com.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.ChildrenDao;
import com.entities.Children;
import com.services.ChildrenServices;

@Repository
@Transactional
public class ChildrenServicesImpl implements ChildrenServices {

	@Autowired
	ChildrenDao childrenDao;
	
	public boolean saveOrUpdate(Children children) {
		return childrenDao.saveOrUpdate(children);
	}

	public List<Children> list() {
		return childrenDao.list();
	}

	public boolean delete(Children children) {
		return childrenDao.delete(children);
	}

}
