package com.servicesImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.CancellationsDao;
import com.entities.Cancellations;
import com.entities.Children;
import com.entities.Contracts;
import com.services.CancellationsServices;

@Repository
@Transactional
public class CancellationsServicesImpl implements CancellationsServices {

	@Autowired
	CancellationsDao cancellationsDao;
	
	public boolean saveOrUpdate(Cancellations cancellations) {
		return cancellationsDao.saveOrUpdate(cancellations);
	}

	public List<Cancellations> list() {
		return cancellationsDao.list();
	}

	public boolean delete(Cancellations cancellations) {
		return cancellationsDao.delete(cancellations);
	}

	public List<Children> getChildren() {
		return cancellationsDao.getChildren();
	}

	public Cancellations findCancellationById(String cancellationsPeriod, Date date, Integer idContract) {
		return cancellationsDao.findCancellationById(cancellationsPeriod, date, idContract);
	}

	public Contracts getContractByIdChild(Integer idChild) {
		return cancellationsDao.getContractByIdChild(idChild);
	}
	
}
