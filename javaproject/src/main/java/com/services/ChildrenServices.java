package com.services;

import java.util.List;

import com.entities.Children;

public interface ChildrenServices {

	public boolean saveOrUpdate(Children children);
	public List<Children> list();
	public boolean delete(Children children);
}
