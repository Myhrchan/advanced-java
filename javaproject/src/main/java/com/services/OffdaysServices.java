package com.services;

import java.util.Date;
import java.util.List;

import com.entities.Nurseries;
import com.entities.Offdays;

public interface OffdaysServices {

	public boolean saveOrUpdate(Offdays offdays);
	public List<Offdays> list();
	public boolean delete(Offdays offdays);
	public int getNurseryFromName(String nurseryName);
	public List<Nurseries> getNurseries();
	public Offdays findOffdayById(String offdayPeriod, Date date, Integer idNursery);
}
