package com.services;

import java.util.Date;
import java.util.List;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Vacations;

public interface VacationsServices {

	public boolean saveOrUpdate(Vacations vacations);
	public List<Vacations> list();
	public boolean delete(Vacations Vacations);
	public List<Children> getChildren();
	public boolean isThereARegularContractForThisChild(Integer idChild);
	public Contracts getRegularContractByChildId(Integer idChild);
	public Vacations findVacationsById(String vacationsPeriod, Date vacationsDay, Integer idContract);
	
}
