package com.services;

import java.util.List;

import com.entities.Nurseries;

public interface NurseriesServices {

	public boolean saveOrUpdate(Nurseries nurseries);
	public List<Nurseries> list();
	public boolean delete(Nurseries nurseries);
	
}
