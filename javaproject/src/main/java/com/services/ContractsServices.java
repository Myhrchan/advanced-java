package com.services;

import java.util.List;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Nurseries;

public interface ContractsServices {

	public boolean saveOrUpdate(Contracts contracts);
	public List<Contracts> list();
	public boolean delete(Contracts contracts);
	public Nurseries getNurseryFromName(String name);
	public Children getChildrenFromName(String firstname, String lastname);
	public List<Nurseries> getNurseries();
	public List<Children> getChildren();

}
