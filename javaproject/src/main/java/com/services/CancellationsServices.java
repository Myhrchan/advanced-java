package com.services;

import java.util.Date;
import java.util.List;

import com.entities.Cancellations;
import com.entities.Children;
import com.entities.Contracts;

public interface CancellationsServices {

	public boolean saveOrUpdate(Cancellations cancellations);
	public List<Cancellations> list();
	public boolean delete(Cancellations cancellations);
	public List<Children> getChildren();
	public Cancellations findCancellationById(String cancellationsPeriod, Date date, Integer idContract);
	public Contracts getContractByIdChild(Integer idChild);
	
}
