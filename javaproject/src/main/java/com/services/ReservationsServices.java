package com.services;

import java.util.Date;
import java.util.List;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Reservations;

public interface ReservationsServices {

	public boolean saveOrUpdate( Reservations reservations);
	public List< Reservations> list();
	public boolean delete(Reservations reservations);
	public int getNurseryFromName(String nurseryName);
	public List<Children> getChildren();
	public Contracts getTemporaryContractByChildId(Integer idChild);
	public boolean isThereATemporaryContractForThisChild(Integer idChild);
	public Reservations findReservationById(String reservationsPeriod, Date date, Integer idContract);
	
}
