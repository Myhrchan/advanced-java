package com.services;

import java.util.Date;
import java.util.List;

import com.entities.Absences;
import com.entities.Children;
import com.entities.Contracts;

public interface AbsencesServices {

	public boolean saveOrUpdate(Absences absences);
	public List<Absences> list();
	public boolean delete(Absences absences);
	public List<Children> getChildren();
	public Absences findAbsenceById(String absencesPeriod, Date date, Integer idContract);
	public Contracts getContractByIdChild(Integer idChild);
	
}
