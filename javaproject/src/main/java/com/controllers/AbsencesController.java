package com.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Absences;
import com.entities.AbsencesId;
import com.entities.Children;
import com.entities.Contracts;
import com.services.AbsencesServices;

@Controller
@RequestMapping("absences")
public class AbsencesController {
	
	@Autowired
	AbsencesServices absencesServices;
	AbsencesId absencesid;
	Contracts contracts;
	Date date;
	boolean justified;
	Children children;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("absences");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdateDate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSavedDate(@RequestParam("absenceDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date absenceDay) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(absenceDay);
		cal.add(Calendar.DATE, 1);
		this.date  = cal.getTime();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate1", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved1(@RequestParam("absencePeriod")String absencePeriod, @RequestParam("justified")boolean justified) {
		this.absencesid = null;
		this.contracts = null;
		
		this.justified = justified;
		this.absencesid = new AbsencesId();
		this.absencesid.setAbsencePeriod(absencePeriod);
		this.absencesid.setAbsenceDay(date);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate2", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved2(Children child) {
		this.children = child;
		this.contracts = absencesServices.getContractByIdChild(child.getIdChild());
		this.absencesid.setIdContract(this.contracts.getIdContract());
		Absences o = new Absences();
		o.setId(this.absencesid);
		o.setContracts(this.contracts);
		o.setJustified(justified);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (absencesServices.saveOrUpdate(o)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Absences absences) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Absences> list = absencesServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/listChildren", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getChildren(Absences absences) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Children> list = absencesServices.getChildren();
		
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		}
		return map;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(@RequestParam("absencesPeriod")String absencesPeriod, @RequestParam("absencesDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date absencesDay, @RequestParam("idContract")Integer idContract) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(absencesDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Absences o = absencesServices.findAbsenceById(absencesPeriod, date, idContract);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (absencesServices.delete(o)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}

