package com.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Vacations;
import com.entities.VacationsId;
import com.services.VacationsServices;

@Controller
@RequestMapping("vacations")
public class VacationsController {
	
	@Autowired
	VacationsServices vacationsServices;
	VacationsId vacationsid;
	Children children;
	Contracts contract;
	Date date;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("vacations");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdateDate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSavedDate(@RequestParam("vacationDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date vacationDay) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(vacationDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate1", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved1(VacationsId vacationsid) {
		this.vacationsid = null;
		this.contract = null;
		
		this.vacationsid = vacationsid;
		this.vacationsid.setVacationDay(date);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate2", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved2(Children child) {
		this.children = child;
		this.contract = vacationsServices.getRegularContractByChildId(child.getIdChild());
		this.vacationsid.setIdContract(this.contract.getIdContract());
		Vacations o = new Vacations();
		o.setId(this.vacationsid);
		o.setContracts(this.contract);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (vacationsServices.saveOrUpdate(o)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Vacations vacations) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Vacations> list = vacationsServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/listChildren", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getChildren(Vacations vacations) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Children> list = vacationsServices.getChildren();
		List<Children> tmp = new ArrayList<Children>();
		for(Children c: list) {
			if(vacationsServices.isThereARegularContractForThisChild(c.getIdChild())) {
				tmp.add(c);
			}
		}
		
		if (tmp != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", tmp);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(@RequestParam("vacationPeriod")String vacationsPeriod, @RequestParam("vacationDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date vacationsDay, @RequestParam("idContract")Integer idContract) {
			
		Calendar cal = Calendar.getInstance();
		cal.setTime(vacationsDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Vacations o = vacationsServices.findVacationsById(vacationsPeriod, date, idContract);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (vacationsServices.delete(o)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}

