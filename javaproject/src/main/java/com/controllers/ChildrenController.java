package com.controllers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Children;
import com.services.ChildrenServices;

@Controller
@RequestMapping("children")
public class ChildrenController {
	
	@Autowired
	ChildrenServices childrenServices;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("children");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved(Children children) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(children.getBirthdate());
		cal.add(Calendar.DATE, 1);
		children.setBirthdate(cal.getTime());
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (childrenServices.saveOrUpdate(children)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Children children) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Children> list = childrenServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(Children children) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (childrenServices.delete(children)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}

