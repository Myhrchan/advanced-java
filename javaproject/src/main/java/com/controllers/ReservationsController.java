package com.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Reservations;
import com.entities.ReservationsId;
import com.services.ReservationsServices;

@Controller
@RequestMapping("reservations")
public class ReservationsController {
	
	@Autowired
	ReservationsServices reservationsServices;
	ReservationsId reservationsid;
	Children children;
	Contracts contract;
	Date date;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("reservations");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdateDate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSavedDate(@RequestParam("reservationDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date reservationDay) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(reservationDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate1", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved1(ReservationsId reservationsid) {
		this.reservationsid = null;
		this.children = null;
		this.contract = null;
		
		this.reservationsid = reservationsid;
		this.reservationsid.setReservationDay(date);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate2", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved2(Children child) {
		this.children = child;
		this.contract = reservationsServices.getTemporaryContractByChildId(child.getIdChild());
		this.reservationsid.setIdContract(this.contract.getIdContract());
		Reservations o = new Reservations();
		o.setId(this.reservationsid);
		o.setContracts(this.contract);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (reservationsServices.saveOrUpdate(o)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Reservations reservations) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Reservations> list = reservationsServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/listChildren", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getChildren(Reservations reservations) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Children> list = reservationsServices.getChildren();
		List<Children> tmp = new ArrayList<Children>();
		for(Children c: list) {
			if(reservationsServices.isThereATemporaryContractForThisChild(c.getIdChild())) {
				tmp.add(c);
			}
		}
		
		if (tmp != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", tmp);
		}
		return map;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(@RequestParam("reservationsPeriod")String reservationsPeriod, @RequestParam("reservationsDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date reservationsDay, @RequestParam("idContract")Integer idContract) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(reservationsDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Reservations o = reservationsServices.findReservationById(reservationsPeriod, this.date, idContract);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (reservationsServices.delete(o)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}

