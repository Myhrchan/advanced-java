package com.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Cancellations;
import com.entities.CancellationsId;
import com.entities.Children;
import com.entities.Contracts;
import com.services.CancellationsServices;

@Controller
@RequestMapping("cancellations")
public class CancellationsController {
	
	@Autowired
	CancellationsServices cancellationsServices;
	CancellationsId cancellationsid;
	Contracts contracts;
	Date date;
	private Children children;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("cancellations");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdateDate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSavedDate(@RequestParam("cancellationDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date cancellationDay) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(cancellationDay);
		cal.add(Calendar.DATE, 1);
		this.date  = cal.getTime();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate1", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved1(CancellationsId cancellationsid) {
		this.cancellationsid = null;
		this.contracts = null;
		
		this.cancellationsid = cancellationsid;
		this.cancellationsid.setCancellationDay(date);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate2", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved2(Children child) {
		this.children = child;
		this.contracts = cancellationsServices.getContractByIdChild(child.getIdChild());
		this.cancellationsid.setIdContract(this.contracts.getIdContract());
		Cancellations o = new Cancellations();
		o.setId(this.cancellationsid);
		o.setContracts(this.contracts);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (cancellationsServices.saveOrUpdate(o)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Cancellations cancellations) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Cancellations> list = cancellationsServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/listChildren", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getChildren(Cancellations cancellations) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Children> list = cancellationsServices.getChildren();
		
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		}
		return map;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(@RequestParam("cancellationsPeriod")String cancellationsPeriod, @RequestParam("cancellationsDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date cancellationsDay, @RequestParam("idContract")Integer idContract) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(cancellationsDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Cancellations o = cancellationsServices.findCancellationById(cancellationsPeriod, date, idContract);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (cancellationsServices.delete(o)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}

