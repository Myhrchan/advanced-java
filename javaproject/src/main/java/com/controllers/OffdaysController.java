package com.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Nurseries;
import com.entities.Offdays;
import com.entities.OffdaysId;
import com.services.OffdaysServices;

@Controller
@RequestMapping("offdays")
public class OffdaysController {
	
	@Autowired
	OffdaysServices offdaysServices;
	OffdaysId offdaysid;
	Nurseries nurseries;
	Date date;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("offdays");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdateDate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSavedDate(@RequestParam("offdayDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date offdayDay) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(offdayDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate1", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved1(OffdaysId offdaysid) {
		this.offdaysid = null;
		this.nurseries = null;
		
		this.offdaysid = offdaysid;
		this.offdaysid.setOffdayDay(date);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate2", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved2(Nurseries nurseries) {
		this.nurseries = nurseries;
		Offdays o = new Offdays();
		o.setId(offdaysid);
		o.setNurseries(nurseries);
		
		System.out.println(o.getId().getOffdayDay());

		o.getId().setIdNursery(offdaysServices.getNurseryFromName(o.getNurseries().getNurseryName()));
		o.getNurseries().setIdNursery(offdaysServices.getNurseryFromName(o.getNurseries().getNurseryName()));

		Map<String, Object> map = new HashMap<String, Object>();
		if (offdaysServices.saveOrUpdate(o)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Offdays offdays) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Offdays> list = offdaysServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/listNurseries", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAllNurseries(Offdays offdays) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Nurseries> list = offdaysServices.getNurseries();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(@RequestParam("offdayPeriod")String offdayPeriod, @RequestParam("offdayDay")@DateTimeFormat(pattern = "yyyy-MM-dd")Date offdayDay, @RequestParam("idNursery")Integer idNursery) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(offdayDay);
		cal.add(Calendar.DATE, 1);
		this.date = cal.getTime();
		
		Offdays o = offdaysServices.findOffdayById(offdayPeriod, date, idNursery);
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (offdaysServices.delete(o)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}

