package com.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Nurseries;
import com.services.NurseriesServices;

@Controller
@RequestMapping("nurseries")
public class NurseriesController {
	@Autowired
	NurseriesServices nurseriesServices;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("nurseries");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved(Nurseries nurseries) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (nurseriesServices.saveOrUpdate(nurseries)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Nurseries nurseries) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Nurseries> list = nurseriesServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(Nurseries nurseries) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (nurseriesServices.delete(nurseries)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}
