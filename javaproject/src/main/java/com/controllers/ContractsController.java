package com.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Nurseries;
import com.services.ContractsServices;

@Controller
@RequestMapping("contracts")
public class ContractsController {
	
	@Autowired
	ContractsServices contractsServices;
	Contracts contract;
	Children child;
	Nurseries nursery;
	Date startdate;
	Date enddate;
	
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ModelAndView getPage() {
		ModelAndView view = new ModelAndView("contracts");
		return view;
	}
	
	@RequestMapping(value = "/saveOrUpdateDate", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSavedDate(@RequestParam("startDate")@DateTimeFormat(pattern = "yyyy-MM-dd")Date startdate, @RequestParam("endDate")@DateTimeFormat(pattern = "yyyy-MM-dd")Date enddate) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(startdate);
		cal.add(Calendar.DATE, 1);
		this.startdate  = cal.getTime();
		
		cal.setTime(enddate);
		cal.add(Calendar.DATE, 1);
		this.enddate  = cal.getTime();
		
		this.startdate = startdate;
		this.enddate = enddate;
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}

	@RequestMapping(value = "/saveOrUpdate1", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved1(Children children) {
		this.child = null;
		this.nursery = null;
		this.contract = null;
		
		this.child = children;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate2", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved2(Nurseries nursery) {
		this.nursery = nursery;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", "200");
		map.put("message", "Your record has been saved successfully");
		return map;
	}
	
	@RequestMapping(value = "/saveOrUpdate3", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSaved3(Contracts contracts) {
		this.contract = contracts;
		this.contract.setStartDate(startdate);
		this.contract.setEndDate(enddate);
		this.contract.setChildren(this.child);
		this.contract.setNurseries(contractsServices.getNurseryFromName(this.nursery.getNurseryName()));
		Map<String, Object> map = new HashMap<String, Object>();
		if (contractsServices.saveOrUpdate(this.contract)) {
			map.put("status", "200");
			map.put("message", "Your record has been saved successfully");
		}
		else {
			map.put("status", "404");
			map.put("message", "The children or nursery does not exist");
		}
		return map;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAll(Contracts contracts) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Contracts> list = contractsServices.list();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/listChildren", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getChildren(Contracts contracts) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Children> list = contractsServices.getChildren();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/listNurseries", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getNurseries(Contracts contracts) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Nurseries> list = contractsServices.getNurseries();
		if (list != null) {
			map.put("status", "200");
			map.put("message", "Data found");
			map.put("data", list);
		} else {
			map.put("status", "404");
			map.put("message", "Data not found");
		}
		return map;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> delete(Contracts contracts) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (contractsServices.delete(contracts)) {
			map.put("status", "200");
			map.put("message", "Your record have been deleted successfully");
		}
		return map;
	}
}

