package com.daoapi;

import java.util.Date;
import java.util.List;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Vacations;

public interface VacationsDao {

public boolean saveOrUpdate(Vacations objects);
	
	public List<Vacations> list();
	
	public boolean delete(Vacations objects);

	public boolean isThereARegularContractForThisChild(Integer idChild);

	public List<Children> getChildren();

	public Contracts getRegularContractByChildId(Integer idChild);

	public Vacations findVacationsById(String vacationsPeriod, Date vacationsDay, Integer idContract);
	
}
