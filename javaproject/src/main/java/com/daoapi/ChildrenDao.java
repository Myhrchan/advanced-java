package com.daoapi;

import java.util.List;

import com.entities.Children;

public interface ChildrenDao {

public boolean saveOrUpdate(Children objects);
	
	public List<Children> list();
	
	public boolean delete(Children objects);
	
}
