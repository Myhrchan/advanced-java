package com.daoapi;

import java.util.Date;
import java.util.List;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Nurseries;
import com.entities.Reservations;

public interface ReservationsDao {

public boolean saveOrUpdate(Reservations objects);
	
	public List<Reservations> list();
	
	public boolean delete(Reservations objects);

	public Nurseries getNurseryFromName(String nurseryName);

	public List<Children> getChildren();

	public Contracts getTemporaryContractByChildId(Integer idChild);

	public boolean isThereATemporaryContractForThisChild(Integer idChild);

	public Reservations findReservationById(String reservationsPeriod, Date date, Integer idContract);
	
}
