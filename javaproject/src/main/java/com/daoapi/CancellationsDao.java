package com.daoapi;

import java.util.Date;
import java.util.List;

import com.entities.Cancellations;
import com.entities.Children;
import com.entities.Contracts;

public interface CancellationsDao {

public boolean saveOrUpdate(Cancellations objects);
	
	public List<Cancellations> list();
	
	public boolean delete(Cancellations objects);

	public List<Children> getChildren();

	public Cancellations findCancellationById(String cancellationsPeriod, Date date, Integer idContract);

	public Contracts getContractByIdChild(Integer idChild);
	
}
