package com.daoapi;

import java.util.List;

import com.entities.Nurseries;

public interface NurseriesDao {

public boolean saveOrUpdate(Nurseries objects);
	
	public List<Nurseries> list();
	
	public boolean delete(Nurseries objects);
	
}
