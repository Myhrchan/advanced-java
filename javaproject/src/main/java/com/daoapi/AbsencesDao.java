package com.daoapi;

import java.util.Date;
import java.util.List;

import com.entities.Absences;
import com.entities.Children;
import com.entities.Contracts;

public interface AbsencesDao {

public boolean saveOrUpdate(Absences objects);
	
	public List<Absences> list();
	
	public boolean delete(Absences objects);

	public List<Children> getChildren();

	public Absences findAbsenceById(String absencesPeriod, Date date, Integer idContract);

	public Contracts getContractByIdChild(Integer idChild);
	
}
