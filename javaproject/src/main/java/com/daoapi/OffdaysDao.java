package com.daoapi;

import java.util.Date;
import java.util.List;

import com.entities.Nurseries;
import com.entities.Offdays;

public interface OffdaysDao {

public boolean saveOrUpdate(Offdays objects);
	
	public List<Offdays> list();
	
	public boolean delete(Offdays objects);	
	
	public Nurseries getNurseryFromName(String nurseryName);
	
	public List<Nurseries> getNurseries();

	public Offdays findOffdayById(String offdayPeriod, Date date, Integer idNursery);
	
}
