package com.daoapi;

import java.util.List;

import com.entities.Children;
import com.entities.Contracts;
import com.entities.Nurseries;

public interface ContractsDao {

public boolean saveOrUpdate(Contracts objects);
	
	public List<Contracts> list();
	
	public boolean delete(Contracts objects);
	
	public Nurseries getNurseryFromName(String name);
	
	public Children getChildrenFromName(String firstname, String lastname);

	public List<Children> getChildren();
	
	public List<Nurseries> getNurseries();
	
}
