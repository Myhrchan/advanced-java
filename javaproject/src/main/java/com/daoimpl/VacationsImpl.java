package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.VacationsDao;
import com.entities.Vacations;

@Repository("VacationsDao")
@Transactional 
public class VacationsImpl extends Generic<Vacations> implements VacationsDao{

	public List<Vacations> list() {
		return this.list(Vacations.class);
	}
}
