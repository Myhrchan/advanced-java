package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.AbsencesDao;
import com.entities.Absences;

@Repository("AbsencesDao")
@Transactional 
public class AbsencesImpl extends Generic<Absences> implements AbsencesDao{

	public List<Absences> list() {
		return this.list(Absences.class);
	}

	
	
}
