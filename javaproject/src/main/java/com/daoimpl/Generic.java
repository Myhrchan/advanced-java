package com.daoimpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.entities.Absences;
import com.entities.Cancellations;
import com.entities.Children;
import com.entities.Contracts;
import com.entities.Nurseries;
import com.entities.Offdays;
import com.entities.Reservations;
import com.entities.Vacations;

@Transactional 
public class Generic<T> {

	@Autowired 
	SessionFactory session;
	
	public boolean saveOrUpdate(T objects) {
		session.getCurrentSession().saveOrUpdate(objects);
		return true;
	}
	
	public List<T> list(Class<T> type) {
		return session.getCurrentSession().createQuery("from "+ type.getSimpleName() +"").list();
	}
	
	public boolean delete(T objects) {
		try {
			session.getCurrentSession().delete(objects);
		} catch(Exception ex) {
			return false;
		}
		return true;
	}
	
	public Nurseries getNurseryFromName(String name) {
		List<Nurseries> n = session.getCurrentSession().createQuery("from Nurseries where nursery_name='"+name+"'").list();
		return n.get(0);
	}
	
	public Children getChildrenFromName(String firstname, String lastname){
		List<Children> n = session.getCurrentSession().createQuery("from Children where firstname='"+firstname+"' and lastname='"+lastname+"'").list();
		return n.get(0);
	}
	
	public List<Nurseries> getNurseries(){
		return session.getCurrentSession().createQuery("from Nurseries").list();
	}
	
	public List<Children> getChildren(){
		return session.getCurrentSession().createQuery("from Children").list();
	}
	
	public Contracts getContractByIdChild(Integer idChild) {
		List<Contracts> n = session.getCurrentSession().createQuery("from Contracts where id_child="+idChild+"").list();
		return n.get(0);
	}
	
	public Contracts getTemporaryContractByChildId(Integer idChild) {
		List<Contracts> n = session.getCurrentSession().createQuery("from Contracts where id_child="+idChild+" and date1 is null").list();
		return n.get(0);
	}
	
	public Contracts getRegularContractByChildId(Integer idChild) {
		List<Contracts> n = session.getCurrentSession().createQuery("from Contracts where id_child="+idChild+" and date1 is not null").list();
		return n.get(0);
	}

	public boolean isThereATemporaryContractForThisChild(Integer idChild) {
		List<Contracts> n = session.getCurrentSession().createQuery("from Contracts where id_child="+idChild+" and date1 = 'null'").list();
		if(!n.isEmpty()) {
			return true;
		}
		return false;
	}

	
	public boolean isThereARegularContractForThisChild(Integer idChild) {
		List<Contracts> n = session.getCurrentSession().createQuery("from Contracts where id_child="+idChild+" and date1 != 'null' ").list();
		if(!n.isEmpty()) {
			return true;
		}
		return false;
	}
	
	public Vacations findVacationsById(String vacationsPeriod, Date vacationsDay, Integer idContract) {
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		List<Vacations> n = session.getCurrentSession().createQuery("from Vacations where vacation_day = '"+ dateformat.format(vacationsDay) +"' and vacation_period = '" + vacationsPeriod + "' and id_contract = " + idContract+"").list();
		return n.get(0);
	}
	
	public Reservations findReservationById(String reservationsPeriod, Date date, Integer idContract) {
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		List<Reservations> n = session.getCurrentSession().createQuery("from Reservations where reservation_day = '"+ dateformat.format(date) +"' and reservation_period = '" +reservationsPeriod + "' and id_contract = " + idContract+"").list();
		return n.get(0);
	}

	public Offdays findOffdayById(String offdaysPeriod, Date date, Integer idNursery) {
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		List<Offdays> n = session.getCurrentSession().createQuery("from Offdays where offday_day = '"+ dateformat.format(date) +"' and offday_period = '" +offdaysPeriod + "' and id_nursery = " + idNursery+"").list();
		return n.get(0);
	}
	
	public Absences findAbsenceById(String absencesPeriod, Date date, Integer idContract) {
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		List<Absences> n = session.getCurrentSession().createQuery("from Absences where absence_day = '"+ dateformat.format(date) +"' and absence_period = '" +absencesPeriod + "' and id_contract = " + idContract+"").list();
		return n.get(0);
	}
	
	public Cancellations findCancellationById(String cancellationsPeriod, Date date, Integer idContract) {
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		List<Cancellations> n = session.getCurrentSession().createQuery("from Cancellations where cancellation_day = '"+ dateformat.format(date) +"' and cancellation_period = '" +cancellationsPeriod + "' and id_contract = " + idContract+"").list();
		return n.get(0);
	}


	
}
