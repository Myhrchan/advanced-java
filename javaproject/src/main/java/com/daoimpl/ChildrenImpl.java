package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.ChildrenDao;
import com.entities.Children;

@Repository("ChildrenDao")
@Transactional 
public class ChildrenImpl extends Generic<Children> implements ChildrenDao{

	public List<Children> list() {
		return this.list(Children.class);
	}

}
