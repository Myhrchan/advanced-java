package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.ContractsDao;
import com.entities.Contracts;

@Repository("ContractsDao")
@Transactional 
public class ContractsImpl extends Generic<Contracts> implements ContractsDao{

	public List<Contracts> list() {
		return this.list(Contracts.class);
	}
	
	
}
