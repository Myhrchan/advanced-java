package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.OffdaysDao;
import com.entities.Offdays;

@Repository("OffdaysDao")
@Transactional 
public class OffdaysImpl extends Generic<Offdays> implements OffdaysDao{

	public List<Offdays> list() {
		return this.list(Offdays.class);
	}
}
