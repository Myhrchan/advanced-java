package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.NurseriesDao;
import com.entities.Nurseries;

@Repository("NurseriesDao")
@Transactional 
public class NurseriesImpl extends Generic<Nurseries> implements NurseriesDao{

	public List<Nurseries> list() {
		return this.list(Nurseries.class);
	}
}
