package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.CancellationsDao;
import com.entities.Cancellations;

@Repository("CancellationsDao")
@Transactional 
public class CancellationsImpl extends Generic<Cancellations> implements CancellationsDao{
	
	public List<Cancellations> list() {
		return this.list(Cancellations.class);
	}
	
}
