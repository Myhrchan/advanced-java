package com.daoimpl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.daoapi.ReservationsDao;
import com.entities.Reservations;

@Repository("ReservationsDao")
@Transactional 
public class ReservationsImpl extends Generic<Reservations> implements ReservationsDao{


	public List<Reservations> list() {
		return this.list(Reservations.class);
	}
}
