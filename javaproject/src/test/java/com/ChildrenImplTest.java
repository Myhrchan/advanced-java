package com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.ChildrenDao;
import com.entities.Children;

public class ChildrenImplTest {

	private static ClassPathXmlApplicationContext context;
	private static ChildrenDao childrenDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		childrenDao = (ChildrenDao) context.getBean("ChildrenDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		try {
			Children newChild = new Children(); 
			newChild.setFirstname("Tristan");
			newChild.setLastname("Rouge");
			SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
			Date birthdate = dateformat.parse("02-04-2017");
			newChild.setBirthdate(birthdate);
			childrenDao.saveOrUpdate(newChild);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testList() {
		List<Children> children = childrenDao.list(); 
		for(Children n : children) {
			System.out.println(n.getIdChild() +" "+ n.getFirstname() +" "+ n.getLastname());
		}	
	}

}
