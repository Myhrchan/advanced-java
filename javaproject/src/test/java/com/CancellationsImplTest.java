package com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.CancellationsDao;
import com.daoapi.ContractsDao;
import com.entities.Cancellations;
import com.entities.CancellationsId;

public class CancellationsImplTest {

	private static ClassPathXmlApplicationContext context;
	private static CancellationsDao CancellationsDao;
	private static ContractsDao contractsDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		CancellationsDao = (CancellationsDao) context.getBean("CancellationsDao");
		contractsDao = (ContractsDao) context.getBean("ContractsDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		try {
			Cancellations newCancellation = new Cancellations();
			SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
			Date day = dateformat.parse("02-04-2018");
			CancellationsId id = new CancellationsId("morning", day, 1);
			newCancellation.setId(id);
			CancellationsDao.saveOrUpdate(newCancellation);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testList() {
		List<Cancellations> Cancellations = CancellationsDao.list(); 
		for(Cancellations n : Cancellations) {
			System.out.println(n.getId());
		}	
	}

}
