package com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.AbsencesDao;
import com.entities.Absences;
import com.entities.AbsencesId;

public class AbsencesImplTest {

	private static ClassPathXmlApplicationContext context;
	private static AbsencesDao absencesDao;
	//private static ContractsDao contractsDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		absencesDao = (AbsencesDao) context.getBean("AbsencesDao");
		//contractsDao = (ContractsDao) context.getBean("ContractsDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		try {
			Absences newAbsence = new Absences(); 
			SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
			Date absence_date = dateformat.parse("02-04-2018");
			AbsencesId id = new AbsencesId("morning", absence_date, 1);
			newAbsence.setId(id);
			newAbsence.setJustified(true);
			absencesDao.saveOrUpdate(newAbsence);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testList() {
		List<Absences> Absences = absencesDao.list(); 
		for(Absences n : Absences) {
			System.out.println(n.getId() +" "+ n.getJustified());
		}	
	}

}
