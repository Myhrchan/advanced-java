package com;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.ContractsDao;
import com.entities.Children;
import com.entities.Contracts;
import com.entities.Nurseries;

public class ContractsImplTest {

	private static ClassPathXmlApplicationContext context;
	private static ContractsDao contractsDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		contractsDao = (ContractsDao) context.getBean("ContractsDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		Children newChild = new Children();
		newChild.setIdChild(1);
		Nurseries newNursery = new Nurseries();
		newNursery.setIdNursery(1);
		Contracts newContract = new Contracts();
		newContract.setIdContract(1);
		newContract.setNurseries(newNursery);
		newContract.setChildren(newChild);
		newContract.setEmail("junitTest@gmail.com");
		contractsDao.saveOrUpdate(newContract);
	}

	@Test
	public void testList() {
		List<Contracts> contracts = contractsDao.list(); 
		for(Contracts n : contracts) {
			System.out.println(n.getIdContract() +" "+ n.getEmail());
		}	
	}
}
