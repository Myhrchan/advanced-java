package com;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.OffdaysDao;
import com.entities.Offdays;
import com.entities.OffdaysId;

public class OffdaysImplTest {

	private static ClassPathXmlApplicationContext context;
	private static OffdaysDao offdaysDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		offdaysDao = (OffdaysDao) context.getBean("OffdaysDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		Offdays newOffday = new Offdays(); 
		//newOffday.setId(new OffdaysId());
		newOffday.setId(new OffdaysId("morning", new Date(), 2));
		offdaysDao.saveOrUpdate(newOffday);
	}

	@Test
	public void testList() {
		List<Offdays> offdays = offdaysDao.list(); 
		for(Offdays n : offdays) {
			System.out.println(n.getId().toString());
		}	}

/*	@Test
	public void testDelete() {
			Offdays newOffday = new Offdays(); 
			Date day = new Date();
			newOffday.setId(new OffdaysId("morning", day, 2)); 
			offdaysDao.delete(newOffday);
	}
*/
}
