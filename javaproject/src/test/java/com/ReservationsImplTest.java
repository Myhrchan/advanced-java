package com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.ContractsDao;
import com.daoapi.ReservationsDao;
import com.entities.Reservations;
import com.entities.ReservationsId;

public class ReservationsImplTest {

	private static ClassPathXmlApplicationContext context;
	private static ReservationsDao ReservationsDao;
	private static ContractsDao contractsDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ReservationsDao = (ReservationsDao) context.getBean("ReservationsDao");
		contractsDao = (ContractsDao) context.getBean("ContractsDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		try {
			Reservations newReservation = new Reservations();
			SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
			Date day = dateformat.parse("02-04-2017");
			ReservationsId id= new ReservationsId("morning",day,1);
			newReservation.setId(id);
			ReservationsDao.saveOrUpdate(newReservation);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testList() {
		List<Reservations> Reservations = ReservationsDao.list(); 
		for(Reservations n : Reservations) {
			System.out.println(n.getId());
		}	
	}

}
