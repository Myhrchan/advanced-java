package com;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.UsersDao;
import com.entities.Users;

public class UsersImplTest {

	private static ClassPathXmlApplicationContext context;
	private static UsersDao usersDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		usersDao = (UsersDao) context.getBean("UsersDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}

	@Test
	public void testSaveOrUpdate() {
		Users newUser = new Users(); 
		newUser.setUserName("DURAND"); 
		newUser.setEmail("durand@gmail.com"); 
		usersDao.saveOrUpdate(newUser);
	}
	
	@Test
	public void testList() {
		List<Users> users = usersDao.list(); 
		for(Users u : users) {
			System.out.println(u.getUserId() +" "+ u.getUserName()+" "+u.getEmail());
		}
	}
	
	@Test
	public void testDelete() {
	/*	Users newUser = new Users(); 
		newUser.setUserName("ALLAM"); 
		newUser.setUserId(1);
		newUser.setEmail("allam@gmail.com"); 
		usersDao.delete(newUser);
*/}
	

}
