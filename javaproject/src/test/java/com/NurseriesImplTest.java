package com;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.NurseriesDao;
import com.entities.Nurseries;

public class NurseriesImplTest {

	private static ClassPathXmlApplicationContext context;
	private static NurseriesDao nurseriesDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		nurseriesDao = (NurseriesDao) context.getBean("NurseriesDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		Nurseries newNursery = new Nurseries(); 
		newNursery.setNurseryName("El Nursery"); 
		nurseriesDao.saveOrUpdate(newNursery);
	}

	@Test
	public void testList() {
		List<Nurseries> nurseries = nurseriesDao.list(); 
		for(Nurseries n : nurseries) {
			System.out.println(n.getIdNursery() +" "+ n.getNurseryName());
		}	}

/*	@Test
	public void testDelete() {
		Nurseries newNursery = new Nurseries(); 
		newNursery.setNurseryName("Le Paradis"); 
		newNursery.setIdNursery(1);
		nurseriesDao.delete(newNursery);
	}
*/
}
