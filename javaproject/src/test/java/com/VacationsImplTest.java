package com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.daoapi.VacationsDao;
import com.entities.Vacations;
import com.entities.VacationsId;

public class VacationsImplTest {

	private static ClassPathXmlApplicationContext context;
	private static VacationsDao vacationsDao;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("applicationContext.xml");
		vacationsDao = (VacationsDao) context.getBean("VacationsDao");
	}

	@After
	public void tearDown() throws Exception {
		 context.close();
	}
	@Test
	public void testSaveOrUpdate() {
		try {
			Vacations newVacation = new Vacations(); 
			SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
			Date day = dateformat.parse("02-04-2018");
			VacationsId id = new VacationsId("morning", day, 1);
			newVacation.setId(id);
			vacationsDao.saveOrUpdate(newVacation);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testList() {
		List<Vacations> vacations = vacationsDao.list(); 
		for(Vacations n : vacations) {
			System.out.println(n.getId().getIdContract() +" "+n.getId().getVacationDay()+" "+n.getId().getVacationPeriod());
		}	
	}
}
