#Java project : nursery application
#Mariane Champalier, Cecile Coton

#Création de la table

drop table if exists Absences;
drop table if exists Cancellations;
drop table if exists Reservations;
drop table if exists Vacations;
drop table if exists Contracts;
drop table if exists Children;
drop table if exists OffDays;
drop table if exists Nurseries;

CREATE TABLE Children (
id_child INT PRIMARY KEY AUTO_INCREMENT,
birthdate DATE not null,
lastname VARCHAR(30),
firstname VARCHAR(30)
)ENGINE = InnoDB;

CREATE TABLE Nurseries (
id_nursery INT PRIMARY KEY AUTO_INCREMENT,
nursery_name VARCHAR(40)
)ENGINE = InnoDB;

CREATE TABLE OffDays (
offday_period VARCHAR(30),
offday_day DATE,
id_nursery INT,
PRIMARY KEY (offday_period, offday_day, id_nursery),
FOREIGN KEY (id_nursery) REFERENCES Nurseries(id_nursery) on delete cascade
)ENGINE = InnoDB;


CREATE TABLE Contracts (
id_contract INT PRIMARY KEY AUTO_INCREMENT,
id_child INT not null,
id_nursery INT not null,
phone VARCHAR(20) CHECK (telephone LIKE "0%"),
email VARCHAR(40) CHECK (email LIKE "%@%"),
date1 VARCHAR(30),
period1 VARCHAR(30),
date2 VARCHAR(30),
period2 VARCHAR(30),
date3 VARCHAR(30),
period3 VARCHAR(30),
date4 VARCHAR(30),
period4 VARCHAR(30),
price_per_hour REAL,
start_date DATE,
end_date DATE,
foreign key (id_child) references Children (id_child) on delete cascade,
foreign key (id_nursery) references Nurseries (id_nursery) on delete cascade
)ENGINE = InnoDB;

CREATE TABLE Vacations (
id_contract INT,
vacation_period VARCHAR(30),
vacation_day DATE,
primary key (vacation_period, vacation_day, id_contract),
foreign key (id_contract) references Contracts (id_contract) on delete cascade
)ENGINE = InnoDB;

CREATE TABLE Reservations (
id_contract INT,
reservation_period VARCHAR(30),
reservation_day DATE,
primary key (reservation_period, reservation_day, id_contract),
foreign key (id_contract) references Contracts (id_contract) on delete cascade
)ENGINE = InnoDB;


CREATE TABLE Absences (
id_contract INT,
absence_period VARCHAR(30),
absence_day DATE,
justified BOOLEAN DEFAULT false,
primary key (absence_period, absence_day, id_contract),
foreign key (id_contract) references Contracts (id_contract) on delete cascade
)ENGINE = InnoDB;

CREATE TABLE Cancellations (
id_contract INT,
cancellation_period VARCHAR(30),
cancellation_day DATE,
primary key (cancellation_period, cancellation_day, id_contract),
foreign key (id_contract) references Contracts (id_contract) on update cascade
)ENGINE = InnoDB;

#Création des entrées

insert into Children(birthdate, firstname, lastname) values ('2010-12-12','Léo','Duval');
insert into Children(birthdate, firstname, lastname) values ('2010-12-03','Marie','Simoni');
insert into Children(birthdate, firstname, lastname) values ('2010-09-14','Lala','Typ');

insert into Nurseries(nursery_name) values ('Nunurs');

insert into OffDays values ('morning','2018-12-25', 1);
insert into OffDays values ('afternoon','2018-12-25', 1);
insert into OffDays values ('morning','2018-01-01', 1);

insert into Contracts(id_child, id_nursery, phone, email, price_per_hour, date1, period1, date2, period2, date3, period3, date4, period4, start_date, end_date)
values (1,1,'0111111111','pape@orange.fr', 10, 'monday', 'morning', 'monday', 'afternoon', 'tuesday', 'morning', 'tuesday', 'afternoon', '2018-06-10', '2019-06-10');
insert into Contracts(id_child, id_nursery, phone, email, price_per_hour, date1, period1, date2, period2, date3, period3, date4, period4, start_date, end_date)
values (2,1,'0111111112','kiwi@orange.fr', 10, 'monday', 'morning', 'monday', 'afternoon', null, null, null, null, '2017-06-10', '2020-06-10');
insert into Contracts(id_child, id_nursery, phone, email, price_per_hour, start_date, end_date)
values (3,1,'0111111113','park@orange.fr', 10, '2018-06-10', '2018-12-10');

insert into Vacations(id_contract, vacation_period, vacation_day) values(1,'morning','2018-04-12');
insert into Vacations(id_contract, vacation_period, vacation_day) values(1,'afternoon','2018-04-12');
insert into Vacations(id_contract, vacation_period, vacation_day) values(2,'morning','2018-10-10');

insert into Reservations(id_contract, reservation_period, reservation_day) values(3,'morning','2018-06-10');
insert into Reservations(id_contract, reservation_period, reservation_day) values(3,'afternoon','2018-06-10');
insert into Reservations(id_contract, reservation_period, reservation_day) values(3,'morning','2018-08-10');

insert into Absences(id_contract, absence_period, absence_day, justified) values (1,'morning', '2018-06-10', false);

insert into Cancellations(id_contract, cancellation_period, cancellation_day) values (3, 'morning', '2018-03-26');
